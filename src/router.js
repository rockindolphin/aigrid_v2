import Vue from 'vue'
import Router from 'vue-router'
import PageIndex from './views/PageIndex';
import PageRegistration from './views/PageRegistration';
import PageBilling from './views/PageBilling';
import PageProfile from './views/PageProfile';
import PageCompany from './views/PageCompany';
import PageFeedback from './views/PageFeedback';
import PageClients from './views/PageClients';
import PageServices from './views/PageServices';
import PageSpeechRecognition from './views/PageSpeechRecognition';
import PageSpeechAnalytics from './views/PageSpeechAnalytics';
import PageProviders from './views/PageProviders';
import PageHelp from './views/PageHelp';
import PageFaq from './views/PageFaq';
import PageSupport from './views/PageSupport';
import PageDocumentation from './views/PageDocumentation';
import PageRobotOperator from './views/PageRobotOperator';
import PageAdministrator from './views/PageAdministrator';

import PageUi from './views/PageUi';

Vue.use(Router);

const router = new Router({
	mode: process.env.VUE_APP_ROUTER_MODE,
	base: process.env.VUE_APP_ROUTER_BASE,
	linkActiveClass: "active-path",
	linkExactActiveClass: "active",
	routes: [
		{
			path: '/',
			name: 'index',
			component: PageIndex,
			meta: {
				breadCrumb: 'Главная',
				parent: null
			}
		},
		{
			path: '/profile',
			name: 'profile',
			component: PageProfile,
			meta: {
				breadCrumb: 'Профиль',
				parent: 'index'
			}
		},
		{
			path: '/profile/billing',
			name: 'billing',
			component: PageBilling,
			meta: {
				breadCrumb: 'Баланс и биллинг',
				parent: 'profile'
			}
		},
		{
			path: '/registration',
			name: 'registration',
			component: PageRegistration,
			meta: {
				breadCrumb: 'Регистрация',
				parent: 'index'
			}
		},
		{
			path: '/company',
			name: 'company',
			component: PageCompany,
			meta: {
				breadCrumb: 'Компания',
				parent: 'index'
			}
		},
		{
			path: '/feedback',
			name: 'feedback',
			component: PageFeedback,
			meta: {
				breadCrumb: 'Написать нам',
				parent: 'index'
			}
		},
		{
			path: '/clients',
			name: 'clients',
			component: PageClients,
			meta: {
				breadCrumb: 'Клиентам',
				parent: 'index'
			}
		},
		{
			path: '/services',
			name: 'services',
			component: PageServices,
			meta: {
				breadCrumb: 'Каталог сервисов',
				parent: 'index'
			}
		},
		{
			path: '/providers',
			name: 'providers',
			component: PageProviders,
			meta: {
				breadCrumb: 'Поставщикам',
				parent: 'index'
			}
		},
		{
			path: '/administrator',
			name: 'administrator',
			component: PageAdministrator,
			meta: {
				breadCrumb: 'Администратору',
				parent: 'index'
			}
		},
		{
			path: '/documentation',
			name: 'documentation',
			component: PageDocumentation,
			meta: {
				breadCrumb: 'Документация',
				parent: 'index'
			}
		},
		{
			path: '/help',
			name: 'help',
			component: PageHelp,
			meta: {
				breadCrumb: 'Помощь',
				parent: 'index'
			}
		},
		{
			path: '/help/faq',
			name: 'faq',
			component: PageFaq,
			meta: {
				breadCrumb: 'FAQ',
				parent: 'help'
			}
		},
		{
			path: '/help/support',
			name: 'support',
			component: PageSupport,
			meta: {
				breadCrumb: 'Поддержка',
				parent: 'help'
			}
		},
		{
			path: '/services/speech-recognition',
			name: 'speech-recognition',
			component: PageSpeechRecognition,
			meta: {
				breadCrumb: 'Распознавание речи',
				parent: 'services'
			}
		},
		{
			path: '/services/speech-analytics',
			name: 'speech-analytics',
			component: PageSpeechAnalytics,
			meta: {
				breadCrumb: 'Речевая аналитика',
				parent: 'services'
			}
		},
		{
			path: '/services/robot-operator',
			name: 'robot-operator',
			component: PageRobotOperator,
			meta: {
				breadCrumb: 'Робот-оператор',
				parent: 'services'
			}
		},
		{
			path: '/ui',
			name: 'ui',
			component: PageUi
		}
	]
});


export default router;
