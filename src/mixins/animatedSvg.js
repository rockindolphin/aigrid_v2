
import { events } from '../events';

export default {
	props: {
		id: {
			type: String,
			default: `uid_${Math.random().toString(36).substring(2, 6) + Math.random().toString(36).substring(2, 6)}`
		},
		animated: {
			type: Boolean,
			default: false
		},
		indefeniteAnimation: {
			type: Boolean,
			default: true
		}
	},
	data() {
		return {
			uid: this.id,
			isAnimated: this.animated,
			indefeniteAnimationEnabled: this.indefeniteAnimation,
			startPlayed: false,
			finishPlayed: false
		}
	},
	computed: {
		isMobile(){
			return !window.matchMedia(`(min-width:${this.$store.state.breakponts.md}px)`).matches;
		},
		isMobileAndAnimated() {
			return this.isMobile && this.isAnimated;
		}
	},
	methods: {
		playStartAnim(){
			if( this.startPlayed ){ return; }
			if (this.$refs.start ){
				this.$refs.start.beginElement();
				this.startPlayed = true;
			}
		},
		playFinishAnim(nextName = null){
			if( this.finishPlayed ){ return; }
			if (this.$refs.finish ){//финишная линия
				this.$refs.finish.beginElement();
				this.$refs.finish.addEventListener('endEvent', () => {
					this.finishPlayed = true;
				});
			}
			if (nextName && this.$refs.finish){//если есть финишная линия
				this.$refs.finish.addEventListener('endEvent', () => {
					this.$evbus.$emit(events.EV_APP_ANIMATED_SVG_START, {
						name: nextName
					});
				});
			}
			if (nextName && !this.$refs.finish) {//ситуация для мобил, когда нет финишной линии
				this.$evbus.$emit(events.EV_APP_ANIMATED_SVG_START, {
					name: nextName
				});
			}
		}
	},
	mounted(){
		this.$evbus.$on(events.EV_APP_ANIMATED_SVG_CHANGE_UID, () => {
			this.uid = `uid_${Math.random().toString(36).substring(2, 6) + Math.random().toString(36).substring(2, 6)}`
		});
		this.$evbus.$on(events.EV_APP_ANIMATED_SVG_ENABLE_ANIMATION, () => {
			this.isAnimated = true;
		});
		this.$evbus.$on(events.EV_APP_ANIMATED_SVG_START, (data) => {
			if( data.name === this.animationName ){
				this.playStartAnim();
			}
		});
		this.$evbus.$on(events.EV_APP_ANIMATED_SVG_FINISH, (data) => {
			if( data.name === this.animationName ){
				this.playFinishAnim(data.next);
			}
		});
		this.$evbus.$on(events.EV_APP_ANIMATED_SVG_PAUSE_INDEFINITE, (data) => {
			if( this.$refs.svg && data.name === this.animationName ){
				if( this.finishPlayed ){
					this.$refs.svg.pauseAnimations();
				}else{
					setTimeout(() => {
						if (this.finishPlayed) {
							this.$refs.svg.pauseAnimations();
						}
					}, 1500);
				}
			}
		});
		this.$evbus.$on(events.EV_APP_ANIMATED_SVG_PLAY_INDEFINITE, (data) => {
			if( this.$refs.svg && data.name === this.animationName ){
				this.$refs.svg.unpauseAnimations();
			}
		});
	}
}