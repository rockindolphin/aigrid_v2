import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
	strict: debug,
	plugins: debug ? [createLogger()] : [],

	state: {
		authorised: false,
		project: {
			title: 'AI Grid',
			description: 'Маркетплейс сервисов<br> искусственного интеллекта для бизнеса',
			copy: '«AIGrid» — Artificial Intelligence Marketplace'
		},
		menu: [
			{
				title: 'Сервисы',
				route_name: 'services',
				header: true,
				header_authorised: true,
				footer: true,
				inner: [
					{
						title: 'Распознавание речи',
						route_name: 'speech-recognition'
					},
					{
						title: 'Речевая аналитика',
						route_name: 'speech-analytics'
					},
					{
						title: 'Робот-оператор',
						route_name: 'robot-operator'
					}
				]
			},
			{
				title: 'Клиентам',
				route_name: 'clients',
				header: true,
				footer: true,
				footer_class: 'd-lg-none'
			},
			{
				title: 'Поставщикам',
				route_name: 'providers',
				header: true,
				footer: true,
				footer_class: 'd-lg-none'
			},
			{
				title: 'Администратору',
				route_name: 'administrator',
				header_authorised: true
			},
			{
				title: 'Написать нам',
				route_name: 'feedback',
				header: true,
				header_authorised: true,
				footer: true,
				footer_class: 'd-lg-none'
			},
			{
				title: 'Помощь',
				route_name: 'help',
				footer: true,
				inner: [
					{
						title: 'FAQ',
						route_name: 'faq'
					},
					{
						title: 'Поддержка',
						route_name: 'support'
					}
				]
			},
			{
				title: 'Документация',
				route_name: 'documentation',
				footer: true
			}
		],
		authorisedMenu: [
			{
				title: 'Профиль',
				route_name: 'profile'
			},
			{
				title: 'Компания',
				route_name: 'company'
			},
			{
				title: 'Баланс и биллинг',
				route_name: 'billing'
			}
		],
		breakponts: {
			xs: 0,
			sm: 375,
			md: 768,
			lg: 992,
			xl: 1240
		},
		isEdge: /Edge/.test(navigator.userAgent),
		isIE: /Trident/.test(navigator.userAgent),
		isSafari: navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1
	},
	getters: {
		copy(state){
			return `© ${new Date().getFullYear()} ${state.project.copy}`
		},
		animEnabled(state){
			return !(state.isEdge || state.isIE);
		}
	},
	mutations: {
		setIsAuthorised(state, isAuthorised) {
			state.authorised = isAuthorised;
		},
	},
	actions: {
		authoriseUser(){
			this.commit('setIsAuthorised', true);
		},
		deauthoriseUser(){
			this.commit('setIsAuthorised', false);
		}
	}
});
