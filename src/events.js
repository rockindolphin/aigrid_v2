export const events = {
    EV_APP_TOGGLE_PAGE_SCROLL: 'toggle-page-scroll',
    EV_APP_OPEN_POPUP: 'open-popup',
    EV_APP_ANIMATED_SVG_START: 'animated-svg-start',
    EV_APP_ANIMATED_SVG_FINISH: 'animated-svg-finish',
    EV_APP_ANIMATED_SVG_CHANGE_UID: 'animated-svg-change-uid',
    EV_APP_ANIMATED_SVG_ENABLE_ANIMATION: 'animated-svg-enable-animation',
    EV_APP_CLOSE_ALL_WINDOWS: 'close-all-windows',
    EV_APP_ANIMATED_SVG_PAUSE_INDEFINITE: 'animated-svg-pause-indefinite',
    EV_APP_ANIMATED_SVG_PLAY_INDEFINITE: 'animated-svg-play-indefinite'
}
