
import IconLogo from './icon_files/IconLogo.vue';
import IconMenuToggle from './icon_files/IconMenuToggle.vue';
import IconChewronDown from './icon_files/IconChewronDown.vue';
import IconChewronLeft from './icon_files/IconChewronLeft.vue';
import IconChewronRight from './icon_files/IconChewronRight.vue';
import IconAttachment from './icon_files/IconAttachment.vue';
import IconDelete from './icon_files/IconDelete.vue';
import IconRefresh from './icon_files/IconRefresh.vue';
import IconDeleteCross from './icon_files/IconDeleteCross.vue';
import IconUpload from './icon_files/IconUpload.vue';
import IconBank from './icon_files/IconBank.vue';
import IconTelemarketing from './icon_files/IconTelemarketing.vue';
import IconFrame from './icon_files/IconFrame.vue';
import IconTelecommunication from './icon_files/IconTelecommunication.vue';
import IconProfit from './icon_files/IconProfit.vue';
import IconSafe from './icon_files/IconSafe.vue';
import IconEasy from './icon_files/IconEasy.vue';
import IconBell from './icon_files/IconBell.vue';
import IconMoreVertical from './icon_files/IconMoreVertical.vue';
import IconClients from './icon_files/IconClients.vue';
import IconCollection from './icon_files/IconCollection.vue';
import IconVsrShort from './icon_files/IconVsrShort.vue';
import IconPdf from './icon_files/IconPdf.vue';
import IconPlusCircle from './icon_files/IconPlusCircle.vue';
import IconClose from './icon_files/IconClose.vue';
import IconGrantAccess from './icon_files/IconGrantAccess.vue';
import IconSpeechRecognitionSmall from './icon_files/IconSpeechRecognitionSmall.vue';
import IconSpeechAnalyticsSmall from './icon_files/IconSpeechAnalyticsSmall.vue';
import IconRobotOperatorSmall from './icon_files/IconRobotOperatorSmall.vue';
import IconMic from './icon_files/IconMic.vue';
import IconWarning from './icon_files/IconWarning.vue';
import IconDone from './icon_files/IconDone.vue';
import IconError from './icon_files/IconError.vue';
import IconInfo from './icon_files/IconInfo.vue';

export default {
	IconLogo,
	IconVsrShort,
	IconPdf,
	IconMenuToggle,
	IconChewronDown,
	IconChewronLeft,
	IconChewronRight,
	IconAttachment,
	IconDelete,
	IconRefresh,
	IconDeleteCross,
	IconGrantAccess,
	IconUpload,
	IconBell,
	IconPlusCircle,
	IconFrame,
	IconClose,
	IconMoreVertical,
	IconClients,
	IconBank,
	IconTelemarketing,
	IconTelecommunication,
	IconProfit,
	IconSafe,
	IconEasy,
	IconCollection,
	IconSpeechRecognitionSmall,
	IconSpeechAnalyticsSmall,
	IconRobotOperatorSmall,
	IconMic,
	IconWarning,
	IconDone,
	IconError,
	IconInfo
}