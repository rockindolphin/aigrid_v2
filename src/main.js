import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store';
import VueScrollmagic from 'vue-scrollmagic';
import './styles/style.scss';
import './styles/style.css';

// for ie11
import 'core-js/es/number';
import { customEventPolyfill } from './libs/customEventPolyfill.js';
customEventPolyfill();

Vue.config.productionTip = false

Vue.prototype.$evbus = new Vue(); // Global event bus

Vue.use(VueScrollmagic);

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
