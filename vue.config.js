const postcssPresetEnv = require('postcss-preset-env');
const postcssMixins = require('postcss-mixins');
const postcss = require('postcss');
const postcssImport = require('postcss-import');
const cssVariables = {
    '--xs': '0px',
    '--sm': '375px',
    '--md': '768px',
    '--lg': '992px',
    '--xl': '1240px'
};
let postcssPresetEnvMedia = {};
Object.keys(cssVariables).map(key => {
    postcssPresetEnvMedia[`${key}-viewport`] = `(min-width: ${cssVariables[key]})`;
})

module.exports = {
    publicPath: process.env.NODE_ENV === 'development' ? '/' : '',
    css: {
        loaderOptions: {
            css: {
                // эти настройки будут переданы в css-loader
            },
            postcss: {
                ident: 'postcss',
                plugins: () => [
                    postcssImport({

                    }),
                    postcssMixins({
                        mixins: {
                            addJsVariablesToRoot(mixin){
                                var rule = postcss.rule({ selector: ':root' });
                                Object.keys(cssVariables).map(key => {
                                    rule.append({
                                        prop: key,
                                        value: cssVariables[key]
                                    });
                                });
                                mixin.replaceWith(rule);
                            },
                            edgeOnly(mixin, prop, value){
                                let rule = `
                                    @supports(-ms-ime-align: auto) {
                                        ${prop}: ${value};
                                    }
                                `;
                                mixin.replaceWith(rule);
                            },
                            cssLock(mixin, prop, breakpoint1, breakpoint2, y1, y2){
                                function breakpointToPx(breakpoint) {
                                    let val = cssVariables[`--${breakpoint}`];
                                    if(val){
                                        return parseInt(val);
                                    }
                                }
                                let x1 = breakpointToPx(breakpoint1),
                                    x2 = breakpointToPx(breakpoint2),
                                    m = (y2 - y1) / (x2 - x1),
                                    b = y1 - m * x1,
                                    rule = '';

                                //toFixed(10) - ie11 тупит при >10 знаков после запятой
                                rule = `${prop}: calc( ${m.toFixed(10)}*100vw ${b > 0 ? '+ ' : '- '} ${Math.abs(b.toFixed(10))}px);`;
                                mixin.replaceWith(rule);
                            }
                        }
                    }),
                    postcssPresetEnv({
                        stage: 0,
                        features: {
                            'custom-properties': {
                                importFrom: [
                                    { customProperties: cssVariables }
                                ],
                                preserve: true
                            },
                            'custom-media-queries': {
                                importFrom: [
                                    { customMedia: postcssPresetEnvMedia }
                                ]
                            }
                        }
                    })
                ]
            }
        }
    }
}